<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading"><h4>Profil</h4></div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-2">
                        <img src="<?php echo URL . 'img/user.png'?>" class="img-responsive" height="150 px" width="150 px">
                    </div>
                    <div class="col-sm-10">
                        <h2><?php echo $nick;?></h2>
                        <h4>e-mail: <?php echo $email;?></h4>
                        <h4>role: <?php echo $role;?></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <hr>

                        <div class="panel-body">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#userPosts">Moje články</a></li>
                                <li><a data-toggle="tab" href="#assignedPosts">Články k hodnocení</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="userPosts" class="tab-pane active">
                                    <?php
                                    foreach ($user_posts as $post) {
                                        if (strlen($post->content) > 666) {
                                            $dots = "...";
                                        } else {
                                            $dots = "";
                                        }
                                        echo '
                                <div class="panel panel-default">
                                    <div class="panel-heading"><b><a href="' . URL . 'post/show_post/' . $post->id . '">' . $post->title . '</a></b>,
                                    autor: ' . $post->autors . '
                                    </div>
                                    <div class="panel-body">
                                        ' . substr($post->content, 0, 666) . '<br>' . $dots . '
                                    </div>
                                </div>';
                                    }
                                    ?>
                                </div>
                                <div id="assignedPosts" class="tab-pane">
                                    <?php
                                    foreach($user_assigned as $post){
                                        if (strlen($post->content) > 666) {
                                            $dots = "...";
                                        } else {
                                            $dots = "";
                                        }
                                        echo '
                                <div class="panel panel-default">
                                    <div class="panel-heading"><b><a href="' . URL . 'review/review_post/' . $post->id . '">' . $post->title . '</a></b>,
                                    autor: ' . $post->autors . '
                                    </div>
                                    <div class="panel-body">
                                        ' . substr($post->content, 0, 666) . '<br>' . $dots . '
                                    </div>
                                </div>';
                                    }
                                    ?>
                                </div>
                            </div>
                    </div>
                </div>
            </div
        </div>
    </div>
</div>