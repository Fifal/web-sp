<?php

/**
 * Class Register
 * Controller pro registraci
 */
class Register extends Controller
{
    /**
     * Zobrazí registrační formulář
     */
    public function index(){
        $title = 'WEB-CONF Registrace';
        require APP . 'view/_templates/header.php';
        require APP . 'view/register/index.php';
        require APP . 'view/_templates/footer.php';
    }

    /**
     * Zpracování formuláře
     */
    public function submit(){
        if(isset($_POST)){
            $nick = $_POST['nick'];
            $email = $_POST['email'];
            $pass = $_POST['pwd'];
            $pass2 = $_POST['pwd2'];

            if($this->loginModel->isNickTaken($nick)){
                $this->error('uživatelské jméno již existuje');
            }
            else{
                if($pass == $pass2){
                    if(strlen($pass)<4){
                        $this->error('Heslo musí mít alespoň 4 znaky.');
                    }
                    else{
                        $this->loginModel->addUser($nick, $email, $pass);
                        $this->success('Byl jste úspěšně zaregistrován, nyní se můžete přihlásit.');
                    }
                }
                else{
                    $this->error('Hesla se neshodují');
                }
            }

        }
        else{
            $this->error('Chyba při odesílání formuláře');
        }
    }

    /**
     * Přesměrování na reg. formulář a zobrazení chyby
     * @param $msg chyba
     */
    public function error($msg){
        $title = 'WEB-CONF Registrace';
        require APP . 'view/_templates/header.php';
        require APP . 'view/register/error.php';
        require APP . 'view/_templates/footer.php';
    }

    /**
     * Přesměrování na přihlašovací formulář
     * @param $msg zpráva
     */
    public function success($msg){
        $title = 'WEB-CONF Home';
        require APP . 'view/_templates/header.php';
        require APP . 'view/login/index.php';
        require APP . 'view/_templates/footer.php';
    }
}

?>