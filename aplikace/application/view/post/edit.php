<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading"><h4>Editovat příspěvek</h4></div>
            <div class="panel-body">
                <form class="form" role="form" action="<?php echo URL . 'post/commit_edit/'.$postID;?>" method="post" id="form" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="name">Název příspěvku:</label>
                        <input type="text" class="form-control" name="title" value="<?php echo $post_title;?>" required>
                    </div>
                    <div class="form-group">
                        <label for="abstract">Abstrakt:</label>
                        <textarea class="form-control" form="form" name="abstract" rows="15" required><?php echo $content;?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="file">PDF Soubor:</label>
                        <input type="file" class="file" name="file">
                    </div>
                    <input type="submit" class="form-control btn btn-success">
                </form>
            </div>
        </div>
    </div>
</div>