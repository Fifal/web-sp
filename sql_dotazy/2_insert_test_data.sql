﻿
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET 

@OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET 

@OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET 

@OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--

--
-- Vyprázdnit tabulku před vkládáním `article`
--

TRUNCATE TABLE 

`webconf`.`article`;
--
-- Vypisuji data pro tabulku `article`
--

INSERT INTO `webconf`.`article` (`id`, 

`title`, `autors`, `content`, `id_post_author`, `date`, `accepted`) VALUES
(1, 'Jak 

dědeček měnil až vyměnil', 'autor1', 'Koupal se bohatý kupec v řece, dostal se do 

hlubiny a tonul. Šel mimo stařík, sedláček-sivoušek, uslyšel křik, přispěchal a kupce 

z vody vytáhl. Kupec nevěděl, jak se staříkovi odsloužiti: pozval ho k sobě do města, 

pohostil ho skvěle a daroval mu kus zlata jako koňská hlava.\r\nVzal zlato sedlák a 

šel domů, a naproti němu koňař celé stádo koni žene: "Vítej, staříku, odkud tě bůh 

vede?" - "Z města od bohatého kupce." - "A co ti kupec dal?" - "Kus zlata jako koňská 

hlava." - "Dej mi zlato a vyber si nejlepšího koně!" - Vybral si stařík nejlepšího 

koně, poděkoval a šel svou cestou.\r\nJde stařík, a naproti němu pastucha voly žene: 

"Vítej, staříku, odkud tě bůh vede?" - "Z města od kupce." - "A co ti kupec dal?" - 

"Kus zlata jako koňská hlava." - "A kde je to zlato?" - "Vyměnil jsem ho za koně!" - 

"Vyměň mi koně za vola, kterého chceš!" - Stařík vybral si vola, poděkoval a šel svou 

cestou.\r\nJde stařeček, a naproti němu ovčák žene ovčí stádo: "Vítej, stařečku, odkud 

tě bůh vede?" - "Od bohatého kupce z města." - "A co ti kupec dal?" - "Kus zlata jako 

koňská hlava." - "A kde je to zlato?" - "Vyměnil jsem za koně." - "A kůň kde?" - 

"Vyměnil jsem za vola." - "Vyměň mi vola za berana, kterého chceš!" - Vzal stařík 

nejlepšího berana, poděkoval a šel svou cestou.\r\nJde stařík, a naproti němu pastýř 

žene vepře: "Vítej, staříku, kdes byl?" - "V městě, u bohatého kupce." - "Co ti kupec 

dal?" - "Kus zlata jako koňská hlava." - "A kde je to zlato?" - "Vyměnil jsem za 

koně." - "A kůň kde?" - "Vyměnil jsem za vola." - "A vůl kde? - "Vyměnil jsem za 

berana." - "Dej mi berana a vezmi si nejlepšího vepře?" - Vybral si stařík vepříka, 

poděkoval pastýřovi a šel svou cestou.\r\nJde stařík, a naproti němu podomní obchodník 

s košem na zádech: "Vítej, staříku! Odkud jdeš?" - "Od kupce, z města." - "A co ti 

kupec dal?" - "Kus zlata jako koňská hlava." - "A kde je to zlato?" - "Vyměnil jsem za 

koně." - "A kůň kde?" - "Vyměnil jsem za vola." - "A vůl kde?" - "Vyměnil jsem za 

berana." - "A beran kde? - "Vyměnil jsem za vepře." - "Vyměň mi vepříka za jehlu a 

vyber si, kterou chceš." - Vybral si stařík nejpěknější jehlu, poděkoval a šel domů. 

Přišel stařík domů, přelézal přes plot a ztratil jehlu.\r\nVyběhla stařenka naproti 

starouškovi: "Ach můj holoubku! Já bych se tu bez tebe byla utrápila. Nu, vypravuj, 

byls u kupce?" - "Byl." - "A co ti kupec dal?" - "Kus zlata jako koňská hlava." - "A 

kde je to zlato?" - "Proměnil jsem ho za koně." - "A kůň kde?" -	"Proměnil jsem 

za vola." - "A kde vůl?" - "Proměnil jsem za berana." - "A beran kde?" - "Proměnil 

jsem za vepříka." - "A vepřík kde?" - "Proměnil jsem za jehlu. Chtěl jsem ti, 

stařenko, přinésti dáreček, přelézal jsem přes plot a ztratil jehlu." - "Nu, 

chválabohu, můj holoubku, žes ty sám ses vrátil. Pojďme do jizby večeřet!"\r\nA nyní 

žijou staroušek se staruškou šťastni i bez zlata.', 5, '2015-12-16 15:57:48', 1),
(2, 

'Boháč a chudák', 'autor1', 'Byli dva bratři, jeden velmi bohatý, druhý velmi chudý. 

Jednou hlídal chudý svému bohatému bratru snopy na poli a seděl pod kupkou; spatřil 

bílou ženu, sbírala klasy, ježto zůstaly na záhonech, a zastrkávala do kupek. Když pak 

až k němu došla, chytil ji za ruku a tázal se ji, co je zač a co tu dělá? ­ "Já jsem 

tvého bratra Štěstí a sbírám ztracené klásky, aby měl víc pšenice." ­ "A prosím tě, 

kdepak je moje Štěstí?" otázal se chudý. ­ "Na východě," řekla bílá žena a zmizela.\r

\nI umínil si chudý, že půjde do světa hledat svého Štěstí. A když jednou zrána už 

odcházel z domova, nenadále vyskočila ze zápecí Bída a plakala i prosila, aby ji vzal 

taky s sebou. "Oj, moje milá!" řekl chudák. "Jsi velmi slabá a cesta daleká – nedošla 

bys; ale mám tu prázdnou lahvičku, umenši se, vlez do ní a já tě ponesu." Bída vlezla 

do lahvičky a on nemeškaje zandal ji zátkou a dobře zavázal. Potom na cestě, když 

přišel k jedné bažině, vyndal lahvičku a vstrčil ji do bahna a tak se Bídy zprostil.

\r\nPo některém čase přišel do velikého města a nějaký pán vzal ho do služby, aby mu 

kopal sklep. "Platu žádného ti nedám," řekl, "ale co kopaje najdeš, bude tvé." Když 

tak nějaký čas kopal, našel hroudu zlata. Dle úmluvy náležela jemu; ale on dal přece 

polovici pánovi a kopal dále. Konečně přišel na železné dveře, a když je otevřel, byl 

tu podzemní sklep a v něm nesmírné bohatství. Tu slyší z jedné skříně ve sklepě hlas: 

"Otevři, pane můj, otevři!". I pozdvihl víko a ze skříně vyskočila krásná panna, celá 

bílá, poklonila se mu a řekla: "Jsem tvoje Štěstí, jehož jsi tak dlouho hledal; od 

nynějška budu s tebou a s tvou rodinou." Nato zmizela. On pak o to své bohatství opět 

se podělil se svým bývalým pánem, i zůstal mimo to nesmírným boháčem a jmění jeho den 

ode dne se množilo. Nikdy však nezapomněl na svou nouzi.\r\nJednoho dne, procházeje se 

po městě, potkal svého bratra, jenž sem přišel za svým obchodem. I vzal ho s sebou 

domů a vypravoval mu obšírně všecky příhody své: jak viděl jeho Štěstí na poli klásky 

sbírat, jak a kde se své Bídy zprostil a jiné věci. Hostil ho u sebe po několik dní a 

potom mu dal na cestu mnoho peněz a ženě i dětem jeho mnoho darů, i rozloučil se s ním 

bratrsky.\r\nAle bratr jeho byl neupřímný a záviděl mu jeho Štěstí. Ubíraje se domů, 

neustále přemýšlel, jak by na bratra svého mohl zas Bídu poslat. A když přijel k tomu 

místu, kde bratr jeho lahvičku v bahně pochoval, hledal, dlouho hledal, až ji zase 

našel a hned otevřel. V okamžení vyskočila Bída ven, a před očima jemu rostouc, začala 

kolem něho radostně skákat, a objavši ho, líbala a děkovala mu, že ji z vězení 

vysvobodil. "A za to vděčna budu tobě i tvé rodině až do smrti a nikdy vás již 

neopustím!"\r\nDarmo se závistník vymlouval, darmo ji posílal k prvnějšímu pánu: 

nikterak nemohl již Bídy z krku pozbýt, ani prodat, ani darovat, ani zakopat, ani 

utopit, neustále mu byla v patách. Zboží, které si domů vezl, pobrali mu na cestě 

loupežníci; dostav se domů žebrotou, nalezl místo svého stavení hromadu popela a z 

pole mu zatím úrodu všecku pobrala voda. A tak nezůstalo závistivému boháči nic nez 

toliko ­ Bída!', 5, '2015-12-16 15:58:10', 0),
(3, 'Dobře tak, že je smrt na světě', 

'autor1', 'Za starých časů, když ještě pán Ježíš a svatý Petr spolu chodili po světě, 

přišli taky jednou navečír k jednomu kováři a prosili o nocleh. Ten kovář je hned 

ochotně přivítal, hodil kladivo, co v ruce držel, pod kovadlinu, a vedl je do sednice; 

nato pak šel a dal ustrojiti dobrou večeři. A když již bylo po večeři, řekl k těm svým 

hostům: „Vidím, že jste po cestě tuze ušlí a že byste si rádi odpočinuli — a bylo to 

dnes taky parno, až se pot z čela lil! — položte se spolu do mé postele a vyspěte se; 

já si zatím lehnu do stodoly na slámu.“ Nato jim dal dobrou noc a odešel. A když bylo 

ráno, dal zase ustrojiti snídani a potom je ještě kus cesty vyprovodil. A když se již 

s nimi loučil, řekl: „Dal jsem, co jsem měl, vezměte málem zavděk!"\r\nTu potáhl svatý 

Petr po straně pána Ježíše za rukáv a pravil: „Pane, cožpak mu za to žádnou odměnu 

nedáš, že je tak hodný člověk a že nás tak rád viděl?“\r\nOdpověděl mu pán Ježíš: 

„Odměna tohoto světa je odměna lichá, já ale mu jinou chystám v nebesích.“ Pak se 

obrátil k tomu kováři a pravil k němu: „Žádej, cokoli chceš, tři prosby budou tobě 

vyplněny.“\r\nTu se ten kovář zaradoval a řekl: „Když je tomu tak, tedy, pane, učiň, 

abych byl ještě sto let živ a zdráv tak jako dnes.“\r\nI řekl jemu pán Ježíš: „Má se 

tobě státi, jak žádáš. A co chceš dále?“\r\nKovář se rozmýšlel a řekl: „Co mám žádati? 

Mně se tak na světě dobře vede; co každý den spotřebuju, svým řemeslem si vždycky 

vydělám. Učiň tedy, abych měl vždycky díla dosti, tak jako až potud.“ I odpověděl mu 

pán Ježíš: „I to se má tobě vyplniti. A co ještě žádáš potřetí?“\r\nTu již milý kovář 

nevěděl, co říci má; ale rozmysliv se, za chvíli řekl: „Inu, když jsi, pane, tak 

dobrý, tedy učiň, kdo sedne na tu stolici za stolem, cos ty na ní u mě seděl, aby byl 

každý přimrazen a nemohl z místa, až já ho propustím.“\r\nSvatý Petr se tomu zasmál, 

ale pán Ježíš pravil: „Staň se, jak jsi řekl?"\r\nNato se rozešli, pán Ježíš a svatý 

Petr šli svou cestou dále a kovář běžel s radostí domů. I stalo se mu, jak pán Ježíš 

přislíbil. Všichni známí okolo něho byli již pomřeli, on ale byl vždy ještě zdráv a 

čerstvý jako ryba, práce měl hojnost a zpíval si od rána do večera.\r\nAle všeho do 

času. Těch sto let konečné také uplynulo a smrt zaklepala na dvéře.\r\n"Kdo to?“ ozval 

se kovář.\r\n„Já jsem to, smrt, již si jdu pro tebe.“\r\n„Pěkně vítám! Toť jsou k nám 

hosti!" řekl kovář, potutelně se usmívaje, „jen dál, jen dál, vzácná paní! Ale buď tak 

dobrá, počkej jen trochu, až si tuhle ta kladiva a kleště srovnám, však jsem hned 

hotov. Zatím se trochu posaď tuhle na stolici; vím beztoho, že jsi ušlá a že máš po 

světě neustále co běhat.“\r\nSmrt se ničeho nenadála; i nedala se dlouho pobízeti a 

posadila se. Tu se dal kovář do hlasitého smíchu a řekl: „Teď si tu seď a nehýbej se 

mi odtud, až já budu chtít!"\r\nSmrt se počala vzpínati, rachotila hnáty a klepala 

dásněmi; ale nebylo nic platno, nemohla z místa a musila sedět jako by ji byl 

přikoval. Kovář se smál, až se za břicho popadal, zavřel dvéře a šel po svém, jsa tomu 

rád, že se již nemusí smrti obávati, maje ji doma chycenou.\r\nZatím ale radost jeho 

dlouho netrvala a brzo shledal, že se zmýlil.\r\nMěl doma pěkného krmného vepříka a 

chtěl si ho na to potěšení zabíti a šunky dáti do komína, neboť velmi rád pěkné uzené 

šunky jídal. I vzal tedy sekyru a dal vepříku do hlavy ránu, až se převalil; ale 

zatímco se shýbal pro nůž, chtěje ho zaříznouti a krev do hrnce chytiti na jitrnice, 

tu se ten vepřík najednou zase zdvihl a roch, roch, roch! pryč utíkal, a dříve nežli 

se kovář ze svého leknutí vzpamatoval, byl tentam.\r\n„Počkej, mrcho, však já tě zase 

dostanu," pravil kovář a zatím šel do chlívka a vytáhl husu. Krmil ji již dvě neděle — 

na posvícení. "Aspoň si na tobě dnes pochutnám,“ řekl sám k sobě, „když mi ty jitrnice 

upláchly,“ a vzav nůž, chtěl ji zaříznouti. Ale kupodivu, z husy neteklo ani kapky 

krve, a jak jí z krku nůž vytáhl, nebylo po ráně již ani památky! Vtom, co se kovář 

tomu tak divil, husa se mu najednou z ruky vymkla a kejha, kejha! letěla za vepřem.\r

\nTo bylo kováři již trochu příliš! Tak dobře dnes pořídil, a neměl by si na pečeni 

pochutnati? Nechal tedy zatím husu husou a vepře vepřem a šel do holubníka a přinesl 

dvě holoubata. I aby jemu snad zase něco takového nevyvedla, vzal sekyru a usekl jim 

na špalku hlavy — oběma jednou ranou. „Nu, vy alespoň máte dost!“ zabrumlal k sobě sám 

a hodil je na zem. Ale div divoucí! Sotvaže na zem dopadly, navlékly se hlavičky zase 

na krky a frr, byli holubí již taky titam. — Tu se najednou kováři v hlavě zablesklo, 

i uhodil si pěstí do čela a řekl: "Ó já osel! Na to jsem já nepomyslel: ono proto 

nemůže nic umříti, že mám já smrt chycenou!“ I zakroutil nad tím hlavou a nebylo mu to 

tuze milé, že by již měl navždycky opustiti ty pěkné šunky a jitrnice, ty chutné 

martinské husy i ta pečená holoubátka. Ale co dělati? Smrt propustiti? — To ne; ta by 

nejprve jemu krk zakroutila. I pomyslil si tedy, že bude teď místo masa jídati hrách a 

kaši a místo pečeně píkati koláče — však ty jsou, když jináč není, taky dost dobré.\r

\nNějaký čas ještě to tak dobře chodilo, dokud byly staré zásoby. Ale přišlo jaro, a 

tu teprva nastala pravá bída! Všichni živočichové, co jich vloni bylo, obživli z jara 

zase, ani jedinkého nechybělo! A k tomu narostlo takové množství mladých, až se všecko 

všudy hemžilo. Ptáci, myši, kobylky, brouci, žížaly a jiná havěť sežrala a zkazila 

všecko obilí na polích a luka byla, jako by je vypálil; stromy na zahradách stály jako 

metly, listí i květ ožrali motýli a housenky — nebylo možná ani jednu zabíti! V 

jezerech a řekách bylo takové množství rybiček, žab, vodních pavouků a jiného hmyzu, 

že voda jimi zasmrádla a nebylo lze se ji napíti; v povětří pak byla mračna komárů, 

much a mušek a na zemi taková síla ošklivého hmyzu, že by byly musily člověka umořiti, 

kdyby byl mohl umříti. I chodili lidé polomrtví, jako stíny, nemohouce ani živi býti, 

ani zahynouti.\r\nTo uviděv ten kovář, jaké neštěstí svou pošetilou žádosti na světě 

způsobil, řekl: „Přece to tak pánbůh dobře učinil, že je ta smrt na svetě!" Šel a sám 

se ji poddal a ji propustil; ta ho ihned taky zadávila. A tak přišlo potom pomalu 

všecko zase do starého pořádku.\r\n', 5, '2015-12-16 15:58:30', 0),
(4, 'Jirka s 

kozou', 'autor1', 'Byl jeden král a měl dceru, a tu nemohl žádný rozesmát, byla pořád 

smutná. Tak ten král povídal, komu se podaří, aby se zasmála, tomu že ji dá. A byl 

jeden pastýř a měl syna, říkali mu Jirka. Ten povídá: "Táto, půjdu taky zkusit jestli 

bych ji rozesmál. Na vás nechci nic, jenom tu kozu." - Ta koza byla taková, když on 

chtěl a řekl: "Kozo, drž!", že každého, kdo šel kolem, držela, a ten pak musel u ní 

zůstat. - A táta řekl: "No tak jdi."\r\nTak Jirka tu kozu vzal a šel, a potká jednoho, 

ten měl nohu na rameně. Jirka povídal: "Ty, pročpak máš tu nohu na rameně?" - A on: 

"Když ji sundám, tak doskočím sto mil." - "A kam jdeš?" - "Na službu, kdo mě vezme." - 

"Tak pojď se mnou."\r\nŠli dále a potkali zas jednoho, měl prkýnko na očích. "Ty, 

pročpak máš na očích to prkýnko?" - A on: "Když to prkýnko zdvihnu, tak vidím sto 

mil." - "A kam jdeš?" - "Na službu, chceš-li mě vzit. - "I proč ne, vezmu. Pojď taky 

se mnou." Ušli kus cesty, potkali třetího kamaráda, ten měl láhev pod paží a místo 

zátky držel v ní palec. "Ty, pročpak tam ten palec držíš?" - "Když ho vytáhnu, 

dostříknu sto mil, a co, chci, všecko zastříknu. Chceš-li, vezmi mě taky do služby, 

může to být k tvému štěstí a k našemu taky." - A Jirka mu na to: "No tak pojď."\r

\nPotom přišli do města, kde byl ten král se smutnou princeznou, a nakoupili si na tu 

kozu pentli. Zašli do jedné hospody, a tam už bylo zařízeno, až takoví lidé přijdou, 

aby jim dali jist a pít, co budou chtít, že to král všecko zaplatí.\r\nJirka a jeho 

kamarádi kozu celou pentlemi ovázali a dali ji do sednice šenkýři, aby se o ni 

postaral. Ten ji dal do přístěnku, kde jeho dcery ležely. Jirka zavolal: "Kozo, 

drž!"\r\nTen hospodský měl tři dcery, a ty ještě nespaly. Tu řekla Manka: "Oh, kdybych 

mohla taky takovou pentli mít! Půjdu a odvážu si nějakou z té kozy." - Ta druhá, 

Dorla, povídá: "Nechoď, on to ráno pozná." - Ale Manka šla přec. A když se dlouho 

nevracela, řekla ta třetí, Káča: "Jdi tam pro ni." - Dorla šla, vidí, Manka stojí u 

kozy. Třepla ji po zádech: "Pojď, nech toho!" a už se od ní nemohla odtrhnout. - Když 

to trvalo hodnou chvíli, volá Káča: "Pojďte už, neodvažte je všecky!" Šla a třepla 

Dorlu po sukni, a už taky nemohla pryč, musela u ní zůstat.\r\nRáno si milý Jirka 

pospíšil a šel pro kozu a vedl to všecko pryč, Káču, Donu i Manku. Šenkýř ještě spal. 

Šli přes ves, rychtář koukal z okna. "I fuj!" povídá, "Kačenko, co to, co to?" Šel a 

popadl ji za ruku, chtěl ji odtrhnout ale zůstal u ní taky. V uličce potkali pastýře, 

jak žene stádo krav, býk se otřel o Káču, uvázl, a Jirka ho taky vedl.\r\nPotom přišli 

před zámek a z toho vyšli ven sloužící, a když celý ten průvod viděli, šli ke králi a 

povídali mu: "Och pane, máme tu takovou zvláštní podívanou: už tu byly všelijaké 

maškary, ale tohle tu ještě nebylo." Hned tu královskou dceru vyvedli nahoru k oknu, 

ona se podívala a zasmála se, až se zámek otřásl.\r\nA teď se Jirky ptali, co je zač. 

On, že je pastýřův syn a že mu říkají Jirka. A oni na to, že to nemůže být, že mu tu 

princeznu nemohou dát, protože je prostého rodu, leda že by ještě něco vykonal. Jirka 

povídá: "A co?" A oni, že sto mil odtud je studánka, jestli z ní za minutu přinese 

koflík vody, tak princeznu dostane. Milý Jirka řekl tomu, co měl nohu na ramene: "Ty 

jsi povídal, když sundáš tadytu nohu, že doskočíš sto mil." - Ten odpověděl: "Ó to já 

snadno dokážu!" Sundal nohu, skočil a byl u studánky. Ale potom chybělo už jen maličko 

do doby, kdy se měl vrátit, a on pořád nepřicházel. Tu řekl Jirka druhému kamarádovi: 

"Ty jsi povídal, když zdvihneš to prkýnko z oči, že uvidíš na sto mil; podívej se, co 

tam dělá." - "Och pane, on tam u studánky leží. Och jemináčku, on tam usnul!" - "To 

bude zle," povídá Jirka, "už má čas se vrátit. Ty třetí, ty jsi povídal, když vytáhneš 

palec z té láhve, že dostříkneš sto mil; chutě stříkni tam, ať vstává." Třetí kamarád 

vytáhl palec z láhve a stříkal daleko, daleko. Jirka povídá tomu druhému: "Podívej se, 

jestli už se tam hýbá nebo co dělá." - "Och pane, už vstává - utírá se - už nabírá 

vodu:" - Potom ten první skočil, a než se nadáli, už tu byl s vodou, zrovna včas.\r

\nAle na zámku řekli, že musí vykonat ještě jeden kousek. Nedaleko ve skále je takové 

zvíře, jednorožec, ten jim mnoho lidi hubí; jestli ho Jirka shladí ze světa, že tu 

princeznu dostane. Vzal si tedy své pomocniky a šli do lesa k té skále, šli a přišli 

ke smrčině. Našli tam tři pelechy, uválené od tří zvířat. Jedno z nich hubilo lidi. 

Jirka a jeho kamarádi si nabrali kamení a šišky a vlezli nahoru na strom. A když si ta 

zvířata lehla, pustili dolů kámen na jedno z nich, na jednorožce. A ten řekl tomu 

druhému zviřeti: "Dej pokoj, nešťouchej mě!" - Ale to odpovědělo: "Já ti nic nedělám." 

- A zas na toho jednorožce zeshona pustili kámen. - "Dej pokoj, už jsi mi to udělal 

podruhé." - "Vždyť já ti nic nedělám!" - Pak se popadli a prali se, až se váleli po 

zemi. A ten jednorožec chtěl to druhé zvíře probodnout; ale ono uskočilo a jednorožec, 

jak se po něm prudce ohnal, zapíchnul se svým rohem do stromu a nemohl ho vyndat ven. 

Jirka se svými kamarády skočil ze smrku dolů, ta dvě zvířata utekla a tomu třetímu, 

jednorožci, usekli hlavu, vzali ji na ramena a nesli ji do zámku.\r\nTam viděli, že 

Jirka zase ten kousek dokázal. "Co budeme dělat? Snad mu přece musíme princeznu dát!" 

- "Ne, pane králi," povídá jeden sloužící, "to nemůže být. Vždyť je prostého rodu, aby 

dostal královskou dceru! My ho musíme ze světa sprovodit." - Král na to přikázal, aby 

poslouchali, co bude Jirka mluvit, podle řeči že poznají, co je zač a jak na něj. Na 

zámku bydlela jedna žena, ta to slyšela a pak Jirkovi povídala: "S tebou nebude dnes 

dobře, chtějí tě sprovodit ze světa." - "I toho se nebojím, už když mi bylo sotva 

dvanáct let, zabil jsem jich jednou ranou dvanáct." Ale to bylo tak, když mu máma 

upekla placku, vlítlo mu na ni dvanáct much a on je naráz zabil.\r\nTo když na zámku 

slyšeli, řekli; "Jinak to nepůjde, než že ho musíme zastřelit!" Potom dali nastoupit 

vojáky, že mu udělají slávu, že ho budou oddávat na nádvoří. Tak ho tam vyvedli a 

vojáci chtěli už do něho vypálit. Ale Jirka řekl tomu, co držel v láhvi místo zátky 

palec: "Chutě vytáhni palec a všechno zastříkni!" - "Ó pane, to já rád udělám." Vytáhl 

palec a všecky je zastříkl, až byli všichni slepí a žádný nic neviděl.\r\nNa zámku 

když viděli, že to jinak nepůjde, řekli mu, aby šel, že mu tu princeznu dají. Potom ho 

oblékli do pěkných královských šatů a byla svatba.\r\nA já jsem na té svatbě taky 

byla: měli tam muziku, zpívali, jedli a pili; bylo masa, koláčů, všeho plné ošatky, a 

vodičky plná vědra. Dnes jsem šla, včera jsem přišla: našla jsem mezi pařezy vejce, 

hodila jsem ho jednomu na hlavu a udělala jsem mu pleš, má ji doposud.', 5, '2015-12-

16 15:58:50', 0),
(5, 'Rozum a štěstí', 'autor2', 'Jednou potkalo se štěstí s Rozumem 

na nějaké lávce. „Vyhni se mi!" řeklo Štěstí. Rozum byl tehdáž ještě nezkušený, 

nevěděl, kdo komu se má vyhýbat; i řekl: „Proč bych já se ti vyhýbal? Nejsi ty lepší 

než já.“\r\n„Lepší je ten,“ odpovědělo Štěstí, „kdo více dokáže. Vidíš-li tam toho 

selského synka, co v poli oře? Vejdi do něho; a pochodí-li s tebou lépe nežli se mnou, 

budu se ti pokaždé slušně z cesty vyhýbat kdy a kdekoli se potkáme.“\r\nRozum k tomu 

svolil a vešel hned oráčovi do hlavy. Jakmile oráč ucítil, že má v hlavě rozum, začal 

rozumovat: „Což musím já do smrti za pluhem chodit? Vždyť mohu taky jinde a snáze 

štěstí svého dojit!“ Nechal orání, složil pluh a jel domů. „Tatíku,“ povídá, „nelíbí 

se mi to sedlačení; budu se raděj učit zahradníkem.“\r\nTatík řekl: „Což jsi se, 

Vaňku, pominul s rozumem?“, ale pak se rozmyslil a povídá: „Nu když chceš, uč se 

spánembohem, dostane tu chalupu po mně tvůj bratr."\r\nVaněk přišel o chalupu; ale 

nedbal na to nic, šel a dal se ke královskému zahradníkovi do učení. Nemnoho mu 

zahradník ukazoval, za to tím více chápal Vaněk. Brzy potom ani zahradníka 

neposlouchal, jak má co dělat a dělal všecko po svém. Nejprve zahradník se mrzel, ale 

pak, vida, že se tak všecko lépe daří, byl spokojen. „Vidím, že máš více rozumu nežli 

já," řekl, a nechal pak už Vaňka zahradničit, jak sám chtěl. V nedlouhém čase zvelebil 

Vaněk zahradu tak, že král veliké z ní měl potěšení a často se v ní s paní královou a 

se svou jedinou dcerou procházel.\r\nTa královská dcera byla panna velmi krásná, ale 

od dvanáctého svého roku přestala mluvit nikdo ani slova od ní neslyšel. Král velice 

se proto rmoutil a dal rozhlásit: kdo způsobí, aby zas mluvila, že bude jejím 

manželem. I hlásilo se mnoho mladých králů, knížat a jiných velkých pánů, jeden po 

druhém; ale jak přišli, tak zas odešli: žádnému se nepodařilo způsobit, aby 

promluvila. "A proč bych já taky svoje štěstí nezkusil?" pomyslil si Vaněk. „Kdoví, 

nepodaří-li se mi ji k tomu přivést, aby odpověděla, když se budu ptát?“\r\nI dal se 

hned u krále ohlásit a král se svými rady dovedl ho do pokoje, kde dcera jeho 

zůstávala. Ta dcera měla pěkného psíčka a měla jej velmi ráda, protože byl velmi 

čiperný: všemu porozuměl, co chtěla mít.\r\nKdyž Vaněk s králem a s těmi rady do 

jejího pokoje vstoupil, dělal, jako by té panny královské ani neviděl; než obrátil se 

k tomu psíčku a povídá: „Slyšel jsem, psíčku, že jsi velmi čiperný, a jdu k tobě o 

radu. Byli jsme tři tovaryši: jeden řezbář, druhý krejčí a já. Jednou jsme šli lesem a 

musili jsme vněm zůstat přes noc. Abychom před vlky byli bezpeční, udělali jsme si 

oheň a umluvili jsme se, aby jeden po druhém hlídal. Nejdříve hlídal řezbář a pro 

ukrácení chvíle vzal špalík i vyřezal z něho pěknou pannu. Když byla hotova, probudil 

krejčího, aby ten zase hlídal. Krejčí, uviděv dřevěnou pannu, ptal se, co to.\r\n‚Jak 

vidíš,‘ řekl řezbář, ‚byla mi dlouhá chvíle a vyřezal jsem ze špalíku pannu; bude-li 

tobě dloubá chvíle, můžeš ji ošatit.‘ Krejči hned vyndal nůžky, jehlu a nit, střihl na 

šaty a dal se do šití; a když byly šaty hotovy, pannu přistrojil. Potom zavolal na mě, 

abych já šel hlídat. I ptám se ho taky, co to má.\r\n‚Jak vidíš,‘ řekl krejčí, 

‚řezbáři byla dlouhá chvíle a vyřezal ze špalíku pannu a já z dlouhé chvíle jsem ji 

ošatil; a bude-li tobě taky dlouhá chvíle, můžeš ji naučit mluvit.‘ I naučil jsem ji 

skutečně do rána mluvit. Ale ráno, když se moji tovaryši probudili, chtěl každý tu 

pannu mít. Řezbář povídá: ,Já ji udělal.‘ Krejčí: ‚Já ji ošatil.‘ A já jsem si taky 

svoje právo hájil. Pověz mi tedy, psíčku, komu z nás ta panna náleží.“\r\nPsíček 

mlčel; ale místo psíčka odpověděla dcera královská: „Komu by jinému náležela než tobě? 

Co do řezbářovy panny bez života? Co do krejčího ošacení bez řeči? Tys ji dal nejlepší 

dar; život a řeč, a proto právem tobě náleží.“\r\n„Samas o sobě rozhodla,“ řekl Vaněk, 

„i tobě dal já zase řeč a nový život, a proto mi taky právem náležíš.“\r\nTehdy řekl 

ten jeden královský rada: „Jeho Milost královská dá tobě hojnou odměnu, že se ti 

podařilo dceři jeho rozvázat jazyk; ale ji si vzít nemůžeš, jsi prostého rodu.“\r\nA 

král řekl: „Jsi prostého rodu, dám ti místo své dcery hojnou odměnu.“\r\nAle Vaněk 

nechtěl o žádné jiné odměně slyšet a řekl: „Král bez výminky slíbil: kdo způsobí, aby 

dcera jeho zas mluvila, že bude jejím manželem. Královské slovo zákon; a chce-li král, 

aby jiní zákonů jeho šetřili, musí je sám napřed zachovávat. A proto mi král musí svou 

dceru dát.“\r\n„Pochopové, svažte ho!“ volal ten rada. „Kdo praví, že něco král musí, 

uráží Milost královskou a je hoden smrti. Vaše královská Milost rač poručit, ať je ten 

zločinec mečem odpraven.“\r\nA král řekl: „Ať je mečem odpraven!“\r\nIhned Vaňka 

svázali a vedli na popravu. Když přišli na místo popravní, už tam na ně Štěstí čekalo 

i řeklo tajně k Rozumu: „Hle, jak ten člověk s tebou pochodil: až má přijít O hlavu! 

Ustup, ať já vejdu na tvé místo!“ Jakmile Štěstí do Vaňka vstoupilo, přelomil se 

katovi meč u samého jílce, jako by jej byl někdo přestřihl; a dříve nežli mu zase 

přinesli jiný, přijel z města na koni trubač, jak by letěl, troubil vesele a točil 

bílou korouhvičkou, a za ním přijel pro Vaňka královský kočár.\r\nA to bylo tak: ta 

královská dcera řekla potom doma otci, že Vaněk přece jen pravdu mluvil a že královské 

slovo nemá se rušit, a je-li Vaněk z prostého rodu, že ho král snadno může knížetem 

udělat. A král řekl: „Máš pravdu, ať je knížetem!“ Ihned pro Vaňka poslali královský 

kočár a místo něho byl odpraven ten rada, který krále na Vaňka popudil.\r\nA když 

potom Vaněk a ta královská dcera spolu jeli od oddavek, nahodil se nějak na té cestě 

Rozum; a uviděv, že by se musel potkat se Štěstím, sklopil hlavu a utíkal stranou, jak 

by ho polil. A od té doby prý Rozum, kdykoli se má potkat se Štěstím, zdaleka se mu 

vyhýbá.', 6, '2015-12-16 15:59:19', 0),
(6, 'Čert a káča', 'autor2', 'V jedné vesnici 

byla stará děvečka, jménem Káča. Měla chaloupku, zahradu a k tomu ještě pár zlatých 

peněz, ale kdyby byla celá v zlatě seděla, nebyl by si ji ani ten nejchudší chasník 

vzal, protože byla jako čert zlá a hubatá. Měla starou matku, a leckdy posluhu 

potřebovala, ale kdyby byl mohl někoho krejcar spasit a ona dukáty platila, nebyl by 

jí toho nejmenšího udělal, protože se hned pro každé slovo soudila a vadila, až to 

bylo na deset honů slyšet. K tomu všemu nebyla hezká, a tak zůstávala na ocet, až jí 

bylo již pomalu čtyřicet let. Jak to z většího dílu ve vesnicích bývá, že je každou 

neděli odpoledne muzika, bylo to i tu; jak se ozvaly u rychtářů aneb v hospodě dudy, 

hned byla plná sednice hochů, v síni a po náspi stála děvčata a u oken děti. Ale 

nejprvnější mezi všemi byla Káča; hoši na děvčata kývali, a ty se ubíraly do kola, ale 

Kačence se toho štěstí co živa byla ani jednou nedostalo, ač by dudákovi třeba sama 

byla zaplatila, než navzdor tomu nevynechala přece ani jednu neděli. Jedenkráte jde 

také a myslí si po cestě: "Tak stará jsem již, a ještě jsem s hochem netancovala, 

není-li to k zlosti? Věru dnes bych tancovala třeba s čertem."\r\nSe zlostí přijde do 

hospody, sedne ke kamnům a dívá se, který kterou k tanci bere. Najednou vejde do dveří 

pán v mysliveckém oděvu, sedne nedaleko Kačenky za stůl a dá si nalít. Děvečka přinese 

pivo, pán je vezme a nese Kačence připít. Kačenka se chvíli v duchu divila, že má ten 

pán pro ní takovou čest, chvilku se upejpala, ale konečně se ještě ráda napila. Pán 

džbánek postaví, vytáhne z kapsy dukát, hodí ho dudákovi a křičí: "Sólo, hoši!" Hoši 

se rozstoupí, a pán si bere Káču k tanci.\r\n"I kýho šlaka, kdopak asi je?" ptají se 

staří a sestrkují hlavy; hoši se ušklebují a děvčata schovávají hlavu jedna za druhou 

a přikrývají hubu zástěrou, aby Káča nezahlídla, že se jí smějou. Ale Káča neviděla 

žádného, ona byla ráda, že tancuje, byť by se jí byl celý svět smál, nebyla by si z 

toho pranic dělala. Celé odpoledne, celý večer tancoval pán jen s Káčou, kupoval 

marcipán i sladkou vodku, a když přišel čas jít domů, vyprovázel ji po vsi.\r\n"Kéž 

bych mohla s vámi tak do smrti tancovat jako dnes," pravila Káča, když se měli 

rozcházet.\r\n"I to se ti může stát, pojď se mnou."\r\n"A kde zůstáváte?"\r\n"Chyť se 

mě kolem krku, já ti povím."\r\nKáča ho popadla, ale v tom okamžení se proměnil pán v 

čerta a letěl s ní rovnou k peklu. U vrat se zastavil a tloukl, kamarádi přišli, 

otevřeli, a když uviděli tovaryše, že je celý upachtěn, chtěli mu ulehčit a Káču z 

něho sundat. Ale ta se držela jako klíště a živou mocí se odtrhnout nedala; chtěj 

nechtěj musel se čert s Káčou na krku k panu Antichristovi odebrat.\r\n"Koho si to 

neseš?" ptal se ho pán.\r\nA tu povídal čert, Jak chodil po zemi, že zaslechl Káčin 

nářek o tanečníka, a mysle ji trochu zastrašit, že se s ní pustil do tance, a pak že 

jí na chvíli také peklo chtěl okázat. "Já nevěděl," dokončil, "že se mě nebude chtít 

pustit."\r\n"Protože jsi hlupák a naučení moje si nepamatuješ," vyjel na něj starý 

pán. "Dříve než s kým co začneš, máš jeho smýšleni znát; kdybys byl na to vzpomněl, 

když tě Káča vyprovázela, nebyl bys ji bral s sebou. Nyní se mi kliď z očí, a hleď, 

jak se jí zbavíš."\r\nPln mrzutosti štrachal se čert s pannou Kačenkou na zem. 

Sliboval jí hory doly, jestli se ho pustí, proklínal ji, ale všecko nic platno. 

Utrmácen, rozzloben přijde se svým břemenem na jednu louku, kde mladý ovčák v 

hrozitánském kožichu zaobalen ovce pásl. Čert byl proměněn v obyčejného člověka, a 

tedy ho ovčák nepoznal. "Kohopak to nesete, příteli?" ptá se důvěrně čerta.\r\n"Ach, 

milý člověče, ledva dýchám. Považte jen, já šel svou cestou, na nic ani nepomysle, tu 

mi skočí ta ženská na krk a nechce se mě živou moci pustit. Chtěl jsem ji nést až do 

nejbližší vesnice a tam ji nějak odbýt, ale nejsem v stavu, nohy pode mnou 

klesají."\r\n"No, počkejte chvilku, já vám od té neodbyty pomohu, ale ne na dlouho, 

protože musím zase pást; asi na půl cesty ji odnesu."\r\n"I to budu rád."\r\n"Slyšíš, 

ty, chyť se mě," křičel ovčák na Káču.\r\nSotva to Káča uslyšela, pustila se čerta a 

chytla se huňatého kožichu. Nyní měl štíhlý ovčák co nést Káču a hrozný velký kožich, 

který si byl ráno od nádvorníka vypůjčil. Avšak ho to také dost brzo omrzelo; i 

myslil, jak by se Káči zbavil. Přijde k rybníku, a tu mu napadne, aby ji tam hodil. 

Ale jak? Kdyby byl mohl kožich i s ni svléknout! Byl mu sice dost volný, a proto se o 

to pomalounku pokoušel, jestli by to šlo. A hle! vyndá jednu ruku, Káča neví nic, 

vyndá druhou, Káča neví ještě nic, odundá první pentličku z knoflíku, oddělá druhou, 

třetí a žblunk - Káča leží v rybníce i s kožichem.\r\nČert nešel za ovčákem, on seděl 

na mezi a pásl ovce, dívaje se, brzo-li se ovčák s Káčou navrátí. Nemusel dlouho 

čekat. Mokrý kožich na rameně pospíchal ovčák k louce, mysle, že bude cizinec již snad 

u vesnice a ovce že jsou samotny. Když se spatřili, koukal jeden na druhého. Čert, že 

jde ovčák bez Káči, a ovčák, že tam pán posud sedí. Když se domluvili, řekl čert k 

ovčákovi: "Já ti děkuju, tys mi velkou stužbu prokázal, neboť bych se byl snad musel 

do soudného dne s Káčou nosit. Nikdy na tebe nezapomenu, a jednou se ti bohatě 

odměním. Abys ale věděl, komu jsi z nouze pomohl, povím ti, že jsem čert."\r\nTo 

dořekl a zmizel. Ovčák zůstal chvíli jako omámený stát, pak pravil sám k sobě: "Jsou-

li všichni tak hloupí jako on, tedy je dobře."\r\nNad tou zemí, kde náš ovčák živ byl, 

panoval mladý kníže. Bohatství měl dost; jsa svobodným pánem nade vším, požíval všeho 

v plné míře. Den ode dne kochal se v rozkoších, jakýchž jen svět podat může, a když 

nastala noc, ozýval se z knížecích síni zpěv rozpustilých hýřivých mladíků. Zem 

spravovali dva správcové, kteří nebyli o nic lepší než sám pán. Co neprohýřil kníže, 

nechali si oni dva, a ubohý lid nevěděl, kde má již peněz nabrat. Kdo měl krásnou 

dceru anebo peníze, ten neměl utěšené chvíle, neboť mohl s jistotou rozkaz očekávat, 

že to kníže vším právem pro sebe požádá, a milost boží tomu, kdo by se vůli jeho 

protivil! Kdož by mohl takového panovníka milovat? Po celé zemi proklínal lid knížete 

i správce země. Jedenkráte, když nevěděl již, co si má vymyslit, povolal kníže 

hvězdáře a poručil, aby jemu i dvěma jeho správcům planety četl. Hvězdář uposlechl a 

zkoumal v hvězdách, jaký konec vezme život tří marnotratníků.\r\n"Odpusť, knížecí 

Milosti," řekl, jsa hotov se svým zkoumáním, "tvému životu i tvým správcům hrozí 

takové nebezpečenství, že se; to vyřknouti obávám."\r\n"Jen mluv, ať je to cokoliv! Ty 

ale zde zůstat musíš, a nevyplní-li se tvá slova, přijdeš o hlavu."\r\n"Já se milerád 

spravedlivému rozkazu podvoluji. Poslyš tedy: než bude druhá čtvrt měsíce, přijde pro 

oba správce čert, v tu a tu hodinu, ten a ten den, a v úplňku přijde pak i pro tebe, 

knížecí Milosti, a všecky tři vás za živa do pekla odnese."\r\n"Do vězení odveďte toho 

lživého šejdíře!" poručil kníže, a služebníci učinili podle rozkazu. V srdci nebylo 

knížeti, jak se stavěl; slova hvězdářova otřásla jím jako hlas soudný. Poprvé se v něm 

ozvalo svědomí! Správce odvezli napolo mrtvé domů, žádný z nich do huby nevzal. 

Sebravše všechno jmění své, sedli do vozů, ujeli na své statky a dali hrady ze všech 

stran zatarasit, aby pro ně čerti přijít nemohli. Kníže se obrátil na pravou cestu, 

žil tiše a pokojně, a zponenáhla se pouštěl do spravování země, doufaje, že se snad 

přece krutý osud nevyplní.\r\nO těch věcech neměl chudý ovčák ani zdání; den jak den 

pásal své stádo a nestaral se pranic, co se ve světě děje. Jednoho dne znenadání stojí 

před nim čert a praví k němu: "Přišel jsem, ovčáčku, abych se ti za tvou službu 

odměnil. Až bude čtvrt měsíce, mám odnést do pekla bývalé správce země, protože 

okrádati chudý lid a knížeti zle radili. Me víš-li co, poněvadž vidím, že se polepší, 

nechám je tu a přitom se ti hned odměním. Až přijde ten a ten den, vejdi do prvního 

hradu, kde bude množství lidu. Až se strhne v hradu křik, služebníci vrata otevřou a 

já pána pryč povedu, tu ke mně přistup a řekni: "Odejdi v tu chvíli, sice bude zle!" 

Já tě poslechnu a půjdu. Potom si ale dej od pána dva pytle zlata dát, a nebude-li 

chtít, řekni jen, že mě zavoláš. Od toho jdi k druhému hradu, a též tak udělej a 

stejný plat požádej. S penězi ale dobře hospodař a užívej jich k dobrému. Až bude 

měsíc v úplňku, musím odnést samého knížete, než to bych ti neradil, abys ho chtěl 

vysvobodit, sice bys musel vlastní svou kůži nastavit." To dořekl a odešel.\r\nOvčák 

si pamatoval každé slovo. Když bylo čtvrt měsíce, vypověděl službu a šel ke hradu, kde 

jeden z těch dvou správců zůstával. Přišel tam právě včas. Zástupy lidstva stály tu a 

dívaly se, až čert pána ponese. Tu se strhne v zámku zoufanlivý křik, vrata se 

otevřou, a černý táhne pána ven, zsinalého a napolo mrtvého. Tu vystoupí z lidu ovčák, 

vezme pána za ruku a čerta odstrčiv volá: "Odejdi, sice bude s tebou zle!" Tu chvíli 

čert zmizí, a potěšený pán líbá ruku ovčákovu, ptaje se ho, co žádá za odměnu. Když 

řekl ovčák, že dva pytle zlata, poručil pán, aby se mu ho tolik bez meškáni vydalo.\r

\nSpokojen šel ovčák k druhému hradu, a též tak dobře jak u prvního pořídil. Rozumí se 

samo sebou že se kníže dost brzo o ovčákovi dověděl, neboť se beztoho pořád poptával, 

jak to s pány dopadlo. Když to všecko uslyšel, poslal vůz se čtyřmi koňmi pro milého 

ovčáka, a když ho přivezli, snažně jej žádal, aby se i nad ním smiloval a z drápů 

pekelných ho vysvobodil.\r\n"Pane můj," odpověděl ovčák, "vám to slíbit nemohu; jedná 

se tu o mě. Vy jste velký hříšník, ale kdybyste se chtěl docela polepšit, spravedlivě, 

dobrotivě a moudře svůj lid spravovat, jak se na knížete sluší a patří, pokusil bych 

se o to, i kdybych měl sám za vás jít do horoucího pekla."\r\nKníže s upřímnou myslí 

všecko slíbil, a ovčák odešel, přislíbiv mu, že se v určitý den vynajde.\r\nSe 

strachem a hrůzou očekávali všude úplněk měsíce. Jak to zprvu knížeti přáli, tak ho 

nyní litovali, neboť od té chvíle, co se polepšil, nemohl si žádný hodnějšího knížete 

přát. Dnové ubíhají, ať je člověk s radostí aneb s žalostí počítá! Než se kníže nadál, 

byl za dveřmi den, ve kterém se vším, co ho těšilo, rozloučit se musel. Černě 

přistrojen, na smrt vyděšený, seděl kníže a očekával buďto ovčáka, anebo čerta. 

Najednou se otevřou dvéře, a černý stoji před ním.\r\n"Stroj se, pane kníže, hodina 

vypršela, já jsem tu pro tebe."\r\nAni slova neodpovídaje, vstal kníže a kráčel za 

čertem na dvůr, kde nesmírné množství lidstva stálo. Tu se prodírá houfem ovčák celý 

upachtěný a běží rovnou k čertu, volaje zdaleka:\r\n"Uteč, uteč, sice bude s tebou 

zle!"\r\n"Jak se můžeš opovážit mě zdržovat? Nevíš-li, co jsem ti řekl?" pošeptal čert 

ovčákovi.\r\n"Blázne, mně tu nejde o krále, ale jde mi o tebe. Káča je živa a ptá se 

po tobě."\r\nJakmile uslyšel čert o Káče, byl hned tentam a nechal knížete na pokoji. 

Ovčák se mu v duchu vysmál, a byl rád, že tou lstí knížete vysvobodil. Za to ho udělal 

kníže svým nejprvnějším dvořenínem a co vlastního bratra jej miloval. Avšak se také s 

dobrou potázal, neboť byl chudý pasák upřímný rádce a správný dvořenín. Ze čtyř pytlů 

zlata nenechal si ani krejcaru; pomohl jím těm, od kterých je správcové vydřeli.\r\n', 

6, '2015-12-16 15:59:37', 0),
(7, 'Řípka', 'autor2', 'Byla jednou jedna babka, a ta 

nasila plný žejdlík řepového semena do jednoho důlku. Za čas vyrostla ze všeho toho 

semena jen jedna řípka, ale tak veliká, že ji ani babka, ani žádný jiný nemohl 

vytrhnout. I šla ta babka na potkání prosit každého, aby jí pomohl vytrhnou tu řípku.

\r\nŠla a potkala jednero: "Jednero, pojď a pomoz mi vytrhnout řípku; až ji vytrhneme, 

hrnec ti jí dám."\r\nJednero šlo,\r\nchytilo se babky,\r\nbabka řípky:\r\ntrhaly, 

trhaly,\r\nale nic nemohly vytrhnout.\r\nI šla babka a potkala dvojimo: "Dvojimo, pojď 

a pomoz mi vytrhnout řípku; až ji vytrhneme, hrnec ti jí dám."\r\nDvojimo šlo,\r

\nchytilo se jednera,\r\njednero babky,\r\nbabka řípky:\r\ntrhaly, trhaly,\r\nale nic 

nemohly vytrhnout.\r\nI šla babka a potkala trojimo: "Trojimo, pojď a pomoz mi 

vytrhnout řípku; až ji vytrhneme, hrnec ti jí dám."\r\nTrojimo šlo,\r\nchytilo se 

dvojima,\r\ndvojimo jednera,\r\njednero babky,\r\nbabka řípky:\r\ntrhaly, trhaly,\r

\nale nic nemohly vytrhnout.\r\nA tak to bylo pořád: babka šla, potkala čtvero, 

patero, šestero, sedmero, osmero, devatero -\r\ntrhaly, trhaly,\r\nale nic nemohly 

vytrhnout.\r\nPotom ještě šla a potkala desatero: "Desatero, pojď a pomoz mi vytrhnout 

řípku; až ji vytrhneme, hrnec ti jí dám."\r\nDesatero šlo,\r\nchytilo se devatera,\r

\ndevatero osmera,\r\nosmero sedmera,\r\nsedmero šestera,\r\nšestero patera,\r\npatero 

čtvera,\r\nčtvero trojima,\r\ntrojimo dvojima,\r\ndvojimo jednera,\r\njednero babky,

\r\nbabka řípky:\r\ntrhaly, trhaly -\r\nřípka se přetrhla,\r\nbabka se převrhla,\r\na 

všichni se svalili v jednu hromadu.', 6, '2015-12-16 15:59:54', 0);


TRUNCATE TABLE `webconf`.`reviews`;
--
-- Vypisuji data pro 


INSERT INTO `webconf`.`reviews` (`id_review`, `id_article`, `id_reviewer`, 

`idea`, `theme`, `note`, `lock_edit`) VALUES
(1, 1, 2, 1, 2, 'Moc pěkné.', 1),
(2, 1, 3, 

5, 5, 'Odpad!', 1),
(3, 1, 4, 1, 1, 'hahahaha. xdddddd', 1),
(4, 2, 2, 5, 5, 'Nic moc.', 

0),
(5, 2, 4, 0, 0, '', 0),
(6, 4, 2, 3, 3, 'Spíše průměrné.', 0),
(7, 5, 4, 1, 4, 'No 

mohlo to být lepší.', 0),
(8, 5, 3, 2, 2, 'Lepší průměr.', 0),
(9, 7, 2, 0, 0, '', 0),
(10, 7, 3, 0, 0, '', 0),
(11, 7, 4, 1, 1, 'Líbilo se mi, palec hore!', 0);

-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Stř 16. pro 2015, 16:06
-- Verze serveru: 5.6.26
-- Verze PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Databáze: `webconf`
--

--
-- Vyprázdnit tabulku před vkládáním `user`
--

TRUNCATE TABLE `webconf`.`user`;
--
-- Vypisuji data pro tabulku `user`
--

INSERT INTO `webconf`.`user` (`id_user`, `nick`, `password`, `email`, `role`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.cz', 1),
(2, 'recenzent1', '6a2cb2d17377a88688d9e12105689c9e', 'recenzent1@recenzent1.cz', 3),
(3, 'recenzent2', 'fd61a704d10b48ab25e22988048b990b', 'recenzent2@recenzent2.cz', 3),
(4, 'recenzent3', 'fd61a704d10b48ab25e22988048b990b', 'recenzent3@recenzent3.cz', 3),
(5, 'autor1', '02efafd2e074f4fbb258c3e07c76fba9', 'autor1@autor1.cz', 2),
(6, 'autor2', 'f1db508c71748e92667e96cae4757625', 'autor2@autor2.cz', 2);


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET 

CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET 

COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
