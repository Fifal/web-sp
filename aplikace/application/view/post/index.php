<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading"><h4>Přidat příspěvek</h4></div>
            <div class="panel-body">
               <form class="form" role="form" action="<?php echo URL . 'post/submit'?>" method="post" id="form" enctype="multipart/form-data">
                   <div class="form-group">
                       <label for="name">Název příspěvku:</label>
                       <input type="text" class="form-control" name="title" required>
                   </div>
                   <div class="form-group">
                       <label for="abstract">Abstrakt:</label>
                       <textarea class="form-control" form="form" name="abstract" rows="15" required></textarea>
                   </div>
                   <div class="form-group">
                       <label for="file">PDF Soubor:</label>
                       <input class="file" type="file" name="file" required>
                   </div>
                   <input type="submit" class="form-control btn btn-success">
               </form>
            </div>
        </div>
    </div>
</div>