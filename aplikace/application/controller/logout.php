<?php

/**
 * Class Logout
 * Controller pro odhlašování
 */
class Logout extends Controller{
    /**
     * Odhlášení
     */
    public function index(){
        $title = 'WEB-CONF Home';
        session_start();
        session_destroy();
        $msg = "Byl jste úspěšně odhlášen";

        require APP . 'view/_templates/header.php';
        require APP . 'view/home/message.php';
        require APP . 'view/_templates/footer.php';
    }
}
?>