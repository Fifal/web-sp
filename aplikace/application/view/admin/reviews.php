<?php if (isset($msg)) {
    echo '<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Upozornění! </strong>' . $msg . '
        </div>
    </div>
</div>';
} ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Hodnocení článků</h4>
            </div>
            <div class="panel-body">
                <?php
                $justone = null;
                if($assignedPosts == null){
                        echo 'Nebylo nalezeno žádné hodnocení.';
                }
                foreach ($assignedPosts as $post) {
                    $title = $this->userModel->getPost($post->id_article);
                    $autor = $title[0]->autors;
                    if ($post->id_article != $justone) {
                        echo '<br><table class="table table-condensed table-striped table-bordered"><thead><th class="col-sm-6"><a href="' . URL . 'post/show_post/' . $post->id_article . '">' . $post->title . '</a>, autor: '.$autor.'</th><th class="col-sm-1">Nápad</th><th class="col-sm-1">Téma</th><th class="col-sm-4">Akce</th></thead>';
                        $reviews = $this->adminModel->getReviews($post->id_article);
                        $justone = $post->id_article;
                        foreach ($reviews as $review) {
                            //if($jedeme != $pica->id_article){
                            // $jedeme = $pica->id_article;
                            $disabledLock = null;
                            $disabledUnlock = null;
                            if ($review->lock_edit == 1) {
                                $disabledLock = "disabled";
                            }
                            if ($review->lock_edit == 0) {
                                $disabledUnlock = "disabled";
                            }
                            if($review->idea == 0){
                                $review->idea = "Nehodnoceno";
                            }
                            if($review->theme == 0){
                                $review->theme = "Nehodnoceno";
                            }
                            echo '<tr><td><a href="'.URL.'admin/show_review/'.$post->id_article.'/'.$review->id_reviewer.'">' .$review->nick . '</a></td><td>' . $review->idea . '</td><td>' . $review->theme . '</td><td><input type="button" class="btn btn-xs btn-danger" value="Uzamknout hodnocení" onClick="location.href=\'' . URL . 'review/lock_edit/' . $review->id_review . '\'" ' . $disabledLock . '> <input type="button" class="btn btn-xs btn-success" value="Odemknout hodnocení" onClick="location.href=\'' . URL . 'review/unlock_edit/' . $review->id_review . '\'" ' . $disabledUnlock . '></td></tr>';
                            // }
                        }
                        if ($review->accepted == 1) {
                            echo '</table><input type="button" class="btn btn-xs btn-success" value="Schváleno" onClick="location.href=\'' . URL . 'post/approve_post/' . $post->id_article . '\'"disabled> ';
                            echo '<input type="button" class="btn btn-xs btn-primary" value="Zamítnout" onClick="location.href=\'' . URL . 'post/reject_post/' . $post->id_article . '\'">';
                        } else {
                            echo '</table><input type="button" class="btn btn-xs btn-success" value="Schválit článek" onClick="location.href=\'' . URL . 'post/approve_post/' . $post->id_article . '\'"> ';
                            echo '<input type="button" class="btn btn-xs btn-primary" value="Zamítnuto" onClick="location.href=\'' . URL . 'post/reject_post/' . $post->id_article . '\'" disabled> ';
                        }
                        echo ' <input type="button" class="btn btn-xs btn-danger" value="Smazat článek" onClick="location.href=\'' . URL . 'post/delete_post/' . $post->id_article . '\'"><hr>';
                    }

                }
                ?>
                </table>
            </div>
        </div>
    </div>
</div>