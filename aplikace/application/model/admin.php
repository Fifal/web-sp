<?php

/**
 * Class AdminModel
 * Funkce používané v administraci
 */
class AdminModel
{
    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }

    /**
     * Vrací všechny uživatele z DB
     * @return mixed
     */
    public function getUsers()
    {
        $sql = "SELECT * FROM `user` JOIN `role` ON `user`.`role`=`role`.`id_role`";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Smaže uživatele z DB
     * @param $id id uživatele
     */
    public function deleteUser($id){
        $id = $this->db->quote($id);
        $sql = "DELETE FROM `user` WHERE `id_user`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Vrací informace o uživateli
     * @param $id id uživatele
     * @return mixed
     */
    public function getUser($id){
        $sql = "SELECT * FROM `user` JOIN `role` ON `user`.`role`=`role`.`id_role` WHERE `id_user`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Vrací informace o uživateli
     * @param $nick nick uživatele
     * @return mixed
     */
    public function getUserByNick($nick){
        $nick = $this->db->quote($nick);
        $sql = "SELECT * FROM `user` JOIN `role` ON `user`.`role`=`role`.`id_role` WHERE `nick`=".$nick;
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Upraví informace o uživateli
     * @param $id id uživatele
     * @param $nick nick
     * @param $email email
     * @param $role role
     */
    public function editUser($id, $nick, $email, $role){
        $nick = $this->db->quote($nick);
        $email = $this->db->quote($email);

        $sql = "UPDATE `user` SET `nick`=".$nick.",`email`=".$email.",`role`=".$role." WHERE `id_user`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Vrací všechny přiřazené články
     * @return mixed
     */
    public function getAssignedPosts(){
        $sql = "SELECT * FROM `article` JOIN `reviews` ON `article`.`id`=`reviews`.`id_article` JOIN `user` ON `reviews`.`id_reviewer`=`user`.`id_user` ORDER BY `reviews`.`id_article`";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Všechny hodnocení uživatele
     * @param $id uživatele
     * @return mixed
     */
    public function getUserAssignedPosts($id){
        $sql = "SELECT * FROM `article` JOIN `reviews` ON `article`.`id`=`reviews`.`id_article` JOIN `user` ON `reviews`.`id_reviewer`=`user`.`id_user` WHERE `reviews`.`id_reviewer`=".$id." ORDER BY `reviews`.`id_article`";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Vrací všechny hodnocení
     * @param $id id článku
     * @return mixed
     */
    public function getReviews($id){
        $sql = "SELECT * FROM `reviews` JOIN `article` ON `article`.`id`=`reviews`.`id_article` JOIN `user` ON `reviews`.`id_reviewer`=`user`.`id_user` WHERE `reviews`.`id_article`= ".$id." ORDER BY `reviews`.`id_article`";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Vrací true pokud uživatel již post nehodnotí
     * @param $id_reviewer id uživatele
     * @param $id_post id článku
     * @return bool
     */
    public function canReview($id_reviewer, $id_post){
        $sql = "SELECT * FROM `reviews` WHERE `id_article`=".$id_post." AND `id_reviewer`=".$id_reviewer;
        $query = $this->db->prepare($sql);
        $query->execute();
        if($query->rowCount()>0){
            return false;
        }
        else{
            return true;
        }
    }

    /**
     * Uzamknutí editace hodnocení
     * @param $id id hodnocení
     */
    public function lockEdit($id){
        $sql = "UPDATE `reviews` SET `lock_edit`=1 WHERE `id_review`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Odemčení editace hodnocení
     * @param $id id hodnocení
     */
    public function unlockEdit($id){
        $sql = "UPDATE `reviews` SET `lock_edit`=0 WHERE `id_review`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Schválení postu
     * @param $id id postu
     */
    public function approvePost($id){
        $sql = "UPDATE `article` SET `accepted`=1 WHERE `id`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Zamítnutí postu
     * @param $id id postu
     */
    public function rejectPost($id){
        $sql = "UPDATE `article` SET `accepted`=0 WHERE `id`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Uzamčení hodnocení pro všechny hodnotitele článku
     * @param $id id článku
     */
    public function lockPost($id){
        $sql = "UPDATE `reviews` SET `lock_edit`=1 WHERE `id_article`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Vrací všechny uživatele s rolí recenzenta
     * @return mixed
     */
    public function getReviewers(){
        $sql = "SELECT * FROM `user` WHERE `role`=3";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Přiřadí hodnocení příspěvku uživateli
     * @param $id_article id postu
     * @param $id_reviewer id uživatele
     */
    public function assignPost($id_article, $id_reviewer){
        $sql = "INSERT INTO `reviews`(`id_review`, `id_article`, `id_reviewer`, `idea`, `theme`, `note`, `lock_edit`) VALUES (DEFAULT,".$id_article.",".$id_reviewer.",0,0,'',0)";
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Zrušení přiřazení hodnocení příspěvku uživateli
     * @param $id_article id postu
     * @param $id_reviewer id uživatele
     */
    public function deleteAssign($id_article, $id_reviewer){
        $sql = "DELETE FROM `reviews` WHERE `id_reviewer`=".$id_reviewer.' AND `id_article`='.$id_article;
        $query = $this->db->prepare($sql);
        $query->execute();
    }
}
?>