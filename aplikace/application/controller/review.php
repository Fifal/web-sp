<?php

/**
 * Class Review
 * Controller pro hodnocení článků
 */
class Review extends Controller{
    public $msg = null;

    /**
     * Hlavní stránka, zobrazí všechny články k hodnocení.
     */
    public function index(){
        session_start();
        if(isset($_SESSION['nick'])) {
            $title = "WEB-CONF Hodnocení";
            $posts_to_review = $this->adminModel->getUserAssignedPosts($_SESSION['id_user']);
            session_abort();
            require APP . 'view/_templates/header.php';
            require APP . 'view/review/index.php';
            require APP . 'view/_templates/footer.php';
        }
        else{
            session_abort();
            $this->home("Pro hodnocení musíte být přihlášen.");
        }
    }

    /**
     * Stránka pro hodnocení příspěvku
     * @param $id id postu
     */
    public function review_post($id){
        session_start();
        $msg = $this->msg;
        if(isset($_SESSION['nick'])){
            $canEdit = false;
            $assigned = $this->adminModel->getUserAssignedPosts($_SESSION['id_user']);
            foreach($assigned as $ass){
                if($ass->id_article==$id){
                    $canEdit = true;
                }
            }
            if($canEdit) {
                $title = "WEB-CONF Hodnocení";
                $post = $this->userModel->getPost($id);
                $post_title = $post[0]->title;
                $autors = $post[0]->autors;
                $content = nl2br($post[0]->content);
                $postID = $id;
                $review = $this->userModel->getPostReview($id, $_SESSION['id_user']);
                $idea = $review[0]->idea;
                $theme = $review[0]->theme;
                $lock_edit = $review[0]->lock_edit;
                $note = nl2br($review[0]->note);

                session_abort();
                require APP . 'view/_templates/header.php';
                require APP . 'view/review/review.php';
                require APP . 'view/_templates/footer.php';
            }
            else{
                session_abort();
                $posts_to_review = $this->adminModel->getUserAssignedPosts($_SESSION['id_user']);
                $msg = "Můžete hodnotit přidělené články.";
                require APP . 'view/_templates/header.php';
                require APP . 'view/review/index.php';
                require APP . 'view/_templates/footer.php';
            }
        }
        else{
            session_abort();
            $this->home("Pro hodnocení musíte být přihlášen.");
        }
    }

    /**
     * Pokud není uživatel přihlášen, přesměruje domů s chybovou hláškou
     * @param $msg zpráva
     */
    private function home($msg){
        $title = 'WEB-CONF Home';
        require APP . 'view/_templates/header.php';
        require APP . 'view/home/message.php';
        require APP . 'view/_templates/footer.php';
    }

    /**
     * Zpracování formuláře
     */
    public function submit(){
        session_start();
        if(isset($_SESSION['nick'])){
            if(isset($_POST['idea'])){
                $id_post = $_POST['id_post'];
                $id_reviewer = $_SESSION['id_user'];
                $idea = $_POST['idea'];
                $theme = $_POST['theme'];
                $note = $_POST['note'];

                $this->userModel->reviewPost($id_post, $id_reviewer,$idea,$theme,$note);
                session_abort();
                $this->msg = "Příspěvek byl úspěšně ohodnocen";
                $this->review_post($id_post);
            }
            else{
                session_abort();
                $this->home("Nastala chyba při odesíláni formuláře.");
            }
        }
    }

    /**
     * Uzamčení hodnocení postu
     * @param $id id postu
     */
    public function lock_edit($id){
        session_start();
        if(isset($_SESSION['nick'])&&$_SESSION['id_role']==1){
            $this->adminModel->lockEdit($id);
            session_abort();
            $assignedPosts = $this->adminModel->getAssignedPosts();
            $reviewers = $this->adminModel->getReviewers();
            $title = 'WEB-CONF Hodnocení článků';
            require APP . 'view/_templates/header.php';
            require APP . 'view/admin/reviews.php';
            require APP . 'view/_templates/footer.php';
        }
    }

    /**
     * Odemčení hodnocení postu
     * @param $id postu
     */
    public function unlock_edit($id){
        session_start();
        if(isset($_SESSION['nick'])&&$_SESSION['id_role']==1){
            $this->adminModel->unlockEdit($id);
            session_abort();
            $assignedPosts = $this->adminModel->getAssignedPosts();
            $reviewers = $this->adminModel->getReviewers();
            $title = 'WEB-CONF Hodnocení článků';
            require APP . 'view/_templates/header.php';
            require APP . 'view/admin/reviews.php';
            require APP . 'view/_templates/footer.php';
        }
    }
}
?>