<?php if (isset($msg)) {
    echo '<div class="row">
    <div class="col-sm-6 col-sm-offset-3">
        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Upozornění! </strong>' . $msg . '
        </div>
    </div>
</div>';
} ?>
<div class="col-sm-6 col-sm-offset-3">
    <div class="panel panel-default">
        <div class="panel-heading">Login</div>
        <div class="panel-body">
            <form class="form" role="form" action="<?php echo URL . 'login/submit/'?>" method="post">
                <div class="form-group">
                    <label for="nick">Nick:</label>
                    <input type="text" class="form-control" name="nick" required>
                </div>
                <div class="form-group">
                    <label for="pwd">Heslo:</label>
                    <input type="password" class="form-control" name="pwd" required>
                </div>
                <button type="submit" name="asdf" class="form-control btn btn-success">Odeslat</button>
            </form>
        </div>
    </div>
</div>
