<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h4>Stránka nenalezena.</h4>
            </div>
            <div class="panel-body">
                <p>Litujeme, ale požadovaná stránka nebyla nalezena.</p>
            </div>
        </div>
    </div>
</div>
