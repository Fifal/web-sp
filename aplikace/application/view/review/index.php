<?php if (isset($msg)) {
    echo '<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Upozornění! </strong>' . $msg . '
        </div>
    </div>
</div>';
} ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading"><h4>Hodnotit příspěvky</h4></div>
            <div class="panel-body">
                <?php
                foreach ($posts_to_review as $post) {
                    if (strlen($post->content) > 666) {
                        $dots = "...";
                    } else {
                        $dots = "";
                    }
                    $get_review = $this->userModel->getPostReview($post->id, $_SESSION['id_user']);
                    if($get_review[0]->lock_edit == 1){
                        echo '
                                <div class="panel panel-default">
                                    <div class="panel-heading"><b><a href="' . URL . 'review/review_post/' . $post->id . '">' . $post->title . '</a> - Hodnocení uzamčeno!</b>,
                                    autor: ' . $post->autors . '<p style="float:right;">'.$post->date.'</p>
                                    </div>
                                    <div class="panel-body">
                                        ' . substr($post->content, 0, 666) . '<br>' . $dots . '
                                    </div>
                                </div>';
                    }
                    else{
                        echo '
                                <div class="panel panel-default">
                                    <div class="panel-heading"><b><a href="' . URL . 'review/review_post/' . $post->id . '">' . $post->title . '</a></b>,
                                    autor: ' . $post->autors . '
                                    </div>
                                    <div class="panel-body">
                                        ' . substr($post->content, 0, 666) . '<br>' . $dots . '
                                    </div>
                                </div>';
                    }
                }
                ?></div>
        </div>
    </div>
</div>